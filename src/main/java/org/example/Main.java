package org.example;

import java.util.Locale;
import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Locale.setDefault(Locale.US);

        Scanner sc =  new Scanner(System.in);

        int n, resto, notasDe100,  notasDe50, notasDe20, notasDe10, notasDe5, notasDe2, notasDe1;

        n = sc.nextInt();

        if( n > 0  && n < 1000000){

            notasDe100 = n /100;
            resto = n % 100;

            notasDe50 = resto / 50;

            resto =  resto % 50;

            notasDe20 = resto / 20;

            resto = resto % 20;

            notasDe10 = resto / 10;

            resto = resto % 10;

            notasDe5 = resto / 5 ;

            resto = resto % 5 ;

            notasDe2 = resto / 2;

            resto = resto % 2;

            notasDe1 = resto;

            System.out.println(n);

            System.out.printf("%d nota(s) de R$ 100,00%n", notasDe100);

            System.out.printf("%d nota(s) de R$ 50,00%n", notasDe50);

            System.out.printf("%d nota(s) de R$ 20,00%n", notasDe20);

            System.out.printf("%d nota(s) de R$ 10,00%n", notasDe10);

            System.out.printf("%d nota(s) de R$ 5,00%n", notasDe5);

            System.out.printf("%d nota(s) de R$ 2,00%n", notasDe2);

            System.out.printf("%d nota(s) de R$ 1,00%n", notasDe1);

        }else {

            System.out.println("Valor fora da faixa");
        }

        sc.close();

    }
}